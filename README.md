# Parcel React Quiz POC

This project uses [this boilerplate](https://github.com/dastasoft/parcel-react-boilerplate). You can get the original README.md file, which contains a more detailed information.

The Quiz structure was inspired on this video tutorial https://www.youtube.com/watch?v=aq-fCtg_gG4
There is also a built in react component called react-quiz-component, but I decided to not use it.

https://www.npmjs.com/package/react-quiz-component

## Requirements

yarn/node

## Install

run 'yarn install --dev'

## Local setup

run 'yarn start'

## Formatting

run 'yarn format'

## Test

ensure the server is running, open a new tab on your terminal.

run 'yarn test'
