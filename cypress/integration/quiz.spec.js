/// <reference types="cypress" />

describe('Quiz asserts', () => {
  before(() => {
    //check the port value, sometimes this port is busy on node.
    cy.visit('http://localhost:1234')
  })

  it.only('Should answer quiz', () => {
    cy.get('.startBtn')
      .click()
      .then(() => {
        let answers = ['True', 'False']
        for (let n = 0; n < 10; n++) {
          cy.get(`.answerBtn:contains('${answers[parseInt(Math.random())]}')`)
            .click()
            .then(() => {
              if (n === 9) {
                //when n=9, it has already answered all questions. Should have a 'play again' button
                cy.get('.playBtn')
                  .click()
                  .then(() => {
                    //just ensure that screen is back to trivia at 1st position.
                    cy.get('.startBtn')
                      .click()
                      .then(() => {
                        cy.get('.counter p').should('have.text', '1 / 10')
                      })
                  })
              }
            })
        }
      })
  })
})
