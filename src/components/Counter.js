import React from 'react'

const Counter = ({ current, max }) => {
  return (
    <div className="counter">
      <p>
        {current} / {max}
      </p>
    </div>
  )
}

export default Counter
