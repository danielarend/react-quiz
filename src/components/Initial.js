import React from 'react'

const Initial = ({ play }) => (
  <div className="start">
    <h2>Welcome to the Trivia challenge</h2>
    <p>You will be presented with 10 True or False questions.</p>
    <p>Can you score 100%?</p>
    <button className="startBtn" onClick={play}>
      Begin
    </button>
  </div>
)

export default Initial
