import React from 'react'

const QuestionBox = ({ category, question, options, selected }) => {
  return (
    <div className="questionBox">
      <p>{category}</p>
      <div className="question">{question}</div>
      {options.map((text, index) => (
        <button
          key={index}
          className="answerBtn"
          onClick={() => {
            selected(question, text)
          }}
        >
          {text}
        </button>
      ))}
    </div>
  )
}

export default QuestionBox
