import React from 'react'

const ResultDetail = ({ question, answer, correct }) => (
  <div className="score-details">
    <p>{question}</p>
    <div className={`result-${answer == correct ? 'yes' : 'no'}`}>
      correct?
      {answer == correct ? 'yes' : 'no'}
    </div>
  </div>
)

export default ResultDetail
