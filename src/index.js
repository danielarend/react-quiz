import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import QuestionBox from './components/QuestionBox'
import Result from './components/Result'
import TriviaService from './triviaService'
import Counter from './components/Counter'
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2,
} from 'react-html-parser'
import ResultDetail from './components/ResultDetail'
import Initial from './components/Initial'
class Trivia extends React.Component {
  state = {
    questionBank: [],
    score: 0,
    responses: 0,
    answers: [],
    current: 0,
    requesting: false,
  }

  getQuestions = () => {
    this.setState({
      requesting: true,
    })

    TriviaService.request({
      url: '/api.php',
      method: 'get',
      params: {
        amount: 10,
        difficulty: 'hard',
        type: 'boolean',
      },
    }).then((response) => {
      response.data.results.forEach((item, i) => {
        item.id = i + 1
      })
      this.setState({
        questionBank: response.data.results,
        requesting: false,
      })
    })
  }
  computeAnswer = (questionKey, question, answer, correct) => {
    if (answer === correct) {
      this.setState({
        score: this.state.score + 1,
      })
    }

    let joined = this.state.answers.concat({
      id: questionKey,
      questionText: question,
      answerText: answer,
      correctAnswer: correct,
    })

    this.setState({
      responses: this.state.responses + 1,
      current: this.state.current + 1,
      answers: joined,
    })
  }
  play = () => {
    //prevent multiple requests on click.
    if (this.state.requesting === false) {
      this.getQuestions()
      this.setState({
        current: 1,
      })
    }
  }
  playAgain = () => {
    this.setState({
      score: 0,
      responses: 0,
      current: 0,
      questionBank: [],
      answers: [],
    })
  }

  render() {
    return (
      <div className="container">
        <div className="title">ADP Trivia Exam</div>

        {this.state.responses == 10 ? (
          <div className="wrapper">
            {this.state.answers.map((response) => (
              <ResultDetail
                question={response.questionText}
                answer={response.answerText}
                correct={response.correctAnswer}
              />
            ))}
            <Result score={this.state.score} playAgain={this.playAgain} />
          </div>
        ) : this.state.questionBank.length > 0 &&
          this.state.current > 0 &&
          this.state.questionBank[0].hasOwnProperty('id') ? (
          this.state.questionBank.map((response) =>
            this.state.current === response.id ? (
              <div className="wrapper" key="0">
                <QuestionBox
                  category={response.category}
                  question={ReactHtmlParser(response.question)}
                  options={['True', 'False']}
                  key={response.id}
                  selected={(question, answer) =>
                    this.computeAnswer(
                      response.id,
                      question,
                      answer,
                      response.correct_answer
                    )
                  }
                />
                <Counter current={this.state.current} max="10" />
              </div>
            ) : null
          )
        ) : (
          <Initial play={this.play} />
        )}
      </div>
    )
  }
}

ReactDOM.render(<Trivia />, document.getElementById('root'))
